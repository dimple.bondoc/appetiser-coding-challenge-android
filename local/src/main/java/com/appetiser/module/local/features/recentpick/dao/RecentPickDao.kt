package com.appetiser.module.local.features.recentpick.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appetiser.module.local.base.BaseDao
import com.appetiser.module.local.features.recentpick.models.RecentPickDB
import io.reactivex.Completable
import io.reactivex.Single

@Dao
abstract class RecentPickDao : BaseDao<RecentPickDB> {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun saveRecentPick(searchResultEntity: RecentPickDB): Completable

    @Query("SELECT * FROM ${RecentPickDB.RECENT_PICK_TABLE_NAME} WHERE wrapper_type = '${RecentPickDB.RECENT_PICK_AUDIOBOOK}'")
    abstract fun getAudioBooks(): Single<List<RecentPickDB>>

    @Query("SELECT * FROM ${RecentPickDB.RECENT_PICK_TABLE_NAME} WHERE wrapper_type = '${RecentPickDB.RECENT_PICK_TRACK}'")
    abstract fun getTracks(): Single<List<RecentPickDB>>
}
