package com.appetiser.module.local.features.recentpick

import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.appetiser.module.local.features.recentpick.dao.RecentPickDao
import com.appetiser.module.local.features.recentpick.models.RecentPickDB
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class RecentPickLocalSourceImpl @Inject constructor(
    private val recentPickDao: RecentPickDao
) : RecentPickLocalSource {

    override fun saveRecentPick(recentPick: SearchResultList): Completable {
        return recentPickDao
            .saveRecentPick(
                RecentPickDB
                    .fromDomain(
                        recentPick
                    )
            )
    }

    override fun getAudioBooks(): Single<List<SearchResultList>> {
        return recentPickDao.getAudioBooks()
            .map {
                RecentPickDB.mapFromEntityList(it)
                    .sortedByDescending { item ->
                        item.stampDate
                    }
            }
    }

    override fun getTracks(): Single<List<SearchResultList>> {
        return recentPickDao.getTracks()
            .map {
                RecentPickDB.mapFromEntityList(it)
                    .sortedByDescending { item ->
                        item.stampDate
                    }
            }
    }
}
