package com.appetiser.module.local.features.recentpick.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.appetiser.module.domain.models.searchresult.SearchResultList
import java.util.*

@Entity(tableName = RecentPickDB.RECENT_PICK_TABLE_NAME, primaryKeys = ["artist_id", "track_id"])
class RecentPickDB(
    @ColumnInfo(name = "wrapper_type") var wrapperType: String,
    @ColumnInfo(name = "kind") var kind: String,
    @ColumnInfo(name = "artist_id") var artistId: Int,
    @ColumnInfo(name = "collection_id") var collectionId: Int,
    @ColumnInfo(name = "track_id") var trackId: Int,
    @ColumnInfo(name = "artist_name") var artistName: String,
    @ColumnInfo(name = "collection_name") var collectionName: String,
    @ColumnInfo(name = "track_name") var trackName: String,
    @ColumnInfo(name = "collection_censored_name") var collectionCensoredName: String,
    @ColumnInfo(name = "track_censored_name") var trackCensoredName: String,
    @ColumnInfo(name = "artwork_url_100") var artworkUrl100: String,
    @ColumnInfo(name = "collection_price") var collectionPrice: Double,
    @ColumnInfo(name = "track_price") var trackPrice: Double,
    @ColumnInfo(name = "release_date") var releaseDate: String,
    @ColumnInfo(name = "collection_explicitness") var collectionExplicitness: String,
    @ColumnInfo(name = "track_explicitness") var trackExplicitness: String,
    @ColumnInfo(name = "disc_count") var discCount: Int,
    @ColumnInfo(name = "disc_number") var discNumber: Int,
    @ColumnInfo(name = "track_count") var trackCount: Int,
    @ColumnInfo(name = "track_number") var trackNumber: Int,
    @ColumnInfo(name = "track_time_millis") var trackTimeMillis: Int,
    @ColumnInfo(name = "country") var country: String,
    @ColumnInfo(name = "currency") var currency: String,
    @ColumnInfo(name = "primary_genre_name") var primaryGenreName: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "long_description") var longDescription: String,
    @ColumnInfo(name = "stamp_date") var stampDate: Long
) {
    companion object {
        const val RECENT_PICK_TABLE_NAME = "recent_pick"
        const val RECENT_PICK_AUDIOBOOK = "audiobook"
        const val RECENT_PICK_TRACK = "track"

        /*fun empty(): RecentPickDB {
            return RecentPickDB()
        }*/

        fun fromDomain(recentPick: SearchResultList): RecentPickDB {
            return with(recentPick) {
                RecentPickDB(
                    wrapperType = recentPick.wrapperType.orEmpty(),
                    kind = recentPick.kind.orEmpty(),
                    artistId = recentPick.artistId ?: 0,
                    collectionId = recentPick.collectionId ?: 0,
                    trackId = recentPick.trackId ?: 0,
                    artistName = recentPick.artistName.orEmpty(),
                    collectionName = recentPick.collectionName.orEmpty(),
                    trackName = recentPick.trackName.orEmpty(),
                    collectionCensoredName = recentPick.collectionCensoredName.orEmpty(),
                    trackCensoredName = recentPick.trackCensoredName.orEmpty(),
                    artworkUrl100 = recentPick.artworkUrl100.orEmpty(),
                    collectionPrice = recentPick.collectionPrice ?: 0.0,
                    trackPrice = recentPick.trackPrice ?: 0.0,
                    releaseDate = recentPick.releaseDate.orEmpty(),
                    collectionExplicitness = recentPick.collectionExplicitness.orEmpty(),
                    trackExplicitness = recentPick.trackExplicitness.orEmpty(),
                    discCount = recentPick.discCount ?: 0,
                    discNumber = recentPick.discNumber ?: 0,
                    trackCount = recentPick.trackCount ?: 0,
                    trackNumber = recentPick.trackNumber ?: 0,
                    trackTimeMillis = recentPick.trackTimeMillis ?: 0,
                    country = recentPick.country.orEmpty(),
                    currency = recentPick.currency.orEmpty(),
                    primaryGenreName = recentPick.primaryGenreName.orEmpty(),
                    description = recentPick.description.orEmpty(),
                    longDescription = recentPick.longDescription.orEmpty(),
                    stampDate = recentPick.stampDate ?: Date().time
                )
            }
        }

        fun toDomain(recentPickDB: RecentPickDB): SearchResultList {
            return with(recentPickDB) {
                SearchResultList(
                    wrapperType = wrapperType,
                    kind = kind,
                    artistId = artistId,
                    collectionId = collectionId,
                    trackId = trackId,
                    artistName = artistName,
                    collectionName = collectionName,
                    trackName = trackName,
                    collectionCensoredName = collectionCensoredName,
                    trackCensoredName = trackCensoredName,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    trackPrice = trackPrice,
                    releaseDate = releaseDate,
                    collectionExplicitness = collectionExplicitness,
                    trackExplicitness = trackExplicitness,
                    discCount = discCount,
                    discNumber = discNumber,
                    trackCount = trackCount,
                    trackNumber = trackNumber,
                    trackTimeMillis = trackTimeMillis,
                    country = country,
                    currency = currency,
                    primaryGenreName = primaryGenreName,
                    description = description,
                    longDescription = longDescription,
                    stampDate = stampDate
                )
            }
        }

        fun mapFromEntityList(searchResultList: List<RecentPickDB>): List<SearchResultList> {
            return searchResultList.map { toDomain(it) }
        }
    }
}
