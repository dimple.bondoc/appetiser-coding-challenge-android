package com.appetiser.module.local.features.session

import android.content.SharedPreferences
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.local.features.token.AccessTokenLocalSource
import com.appetiser.module.local.features.user.UserLocalSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class SessionLocalSourceImpl @Inject constructor(
    private val userLocalSource: UserLocalSource,
    private val accessTokenLocalSource: AccessTokenLocalSource,
    private val sharedPreferences: SharedPreferences
) : SessionLocalSource {

    companion object {
        private const val PREF_RECOMMEND_UPDATE_TIMESTAMP = "PREF_RECOMMEND_UPDATE_TIMESTAMP"
        private const val PREF_HAS_SKIPPED_EMAIL_VERIFICATION = "PREF_HAS_SKIPPED_EMAIL_VERIFICATION"
    }

    var session: Session? = null

    override fun getSession(): Single<Session> {
        if (session == null) {
            return getSessionFromDb()
                .doOnSuccess { this.session = it }
        }

        return Single.just(session)
    }

    override fun saveSession(session: Session): Single<Session> {
        return Single
            .zip(
                userLocalSource.saveUser(session.user),
                accessTokenLocalSource.saveAccessToken(session.accessToken),
                { user, accessToken ->
                    Pair(user, accessToken)
                }
            )
            .map { pair ->
                saveSessionFlags(session)
                pair
            }
            .map { pair ->
                session.user = pair.first
                session.accessToken = pair.second

                this.session = session
                this.session
            }
    }

    override fun getUserToken(): String {
        return accessTokenLocalSource.getAccessTokenFromPref()
    }

    private fun getSessionFromDb(): Single<Session> {
        return Single
            .zip(
                userLocalSource.getUser(),
                accessTokenLocalSource.getAccessToken(),
                { user, accessToken ->
                    val session = Session(user, accessToken)
                    getSessionFlags(session)

                    session
                }
            )
    }

    override fun clearSession(): Completable {
        clearSessionFlags()

        return userLocalSource
            .deleteUser()
            .andThen(accessTokenLocalSource.deleteToken())
            .doOnComplete {
                this.session = null
            }
    }

    private fun saveSessionFlags(session: Session) {
        sharedPreferences
            .edit()
            .apply {
                putLong(PREF_RECOMMEND_UPDATE_TIMESTAMP, session.recommendUpdateTimeStamp)
                putBoolean(PREF_HAS_SKIPPED_EMAIL_VERIFICATION, session.hasSkippedEmailVerification)
                apply()
            }
    }

    private fun getSessionFlags(session: Session) {
        session.recommendUpdateTimeStamp =
            sharedPreferences.getLong(PREF_RECOMMEND_UPDATE_TIMESTAMP, 0L)
        session.hasSkippedEmailVerification =
            sharedPreferences.getBoolean(PREF_HAS_SKIPPED_EMAIL_VERIFICATION, false)
    }

    private fun clearSessionFlags() {
        sharedPreferences
            .edit()
            .apply {
                putLong(PREF_RECOMMEND_UPDATE_TIMESTAMP, 0L)
                putBoolean(PREF_HAS_SKIPPED_EMAIL_VERIFICATION, false)
                apply()
            }
    }
}
