package com.appetiser.module.local.features.recentpick

import com.appetiser.module.domain.models.searchresult.SearchResultList
import io.reactivex.Completable
import io.reactivex.Single

interface RecentPickLocalSource {
    fun saveRecentPick(recentPick: SearchResultList): Completable
    fun getAudioBooks(): Single<List<SearchResultList>>
    fun getTracks(): Single<List<SearchResultList>>
}
