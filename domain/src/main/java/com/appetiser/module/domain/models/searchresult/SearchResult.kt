package com.appetiser.module.domain.models.searchresult

data class SearchResult(
    var resultCount : Int? = null,
    var results: List<SearchResultList>? = null
)
