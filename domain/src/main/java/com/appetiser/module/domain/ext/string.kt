package com.appetiser.module.domain.ext

import androidx.core.text.HtmlCompat

fun String.formattedString() : String {
    val str = this
    return HtmlCompat.fromHtml(str, HtmlCompat.FROM_HTML_MODE_LEGACY).toString()
}

