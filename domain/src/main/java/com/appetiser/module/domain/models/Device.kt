package com.appetiser.module.domain.models

data class Device(var width: Int, var height: Int)
