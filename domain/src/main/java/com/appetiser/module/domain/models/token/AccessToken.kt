package com.appetiser.module.domain.models.token

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class AccessToken(
    val token: String = "",
    val refresh: String = "",
    val tokenType: String = "",
    val expiresIn: String = ""
) : Parcelable {

    val bearerToken get() = "Bearer $token"
}
