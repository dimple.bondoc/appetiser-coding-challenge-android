package com.appetiser.module.domain.models.session

import android.os.Parcelable
import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.domain.models.token.AccessToken
import com.appetiser.module.domain.models.user.User
import kotlinx.parcelize.Parcelize

@Parcelize
data class Session(
    var user: User,
    var accessToken: AccessToken,
    var updateGate: UpdateGate? = null,
    var recommendUpdateTimeStamp: Long = 0L,
    var hasSkippedEmailVerification: Boolean = false
) : Parcelable