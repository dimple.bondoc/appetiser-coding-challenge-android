package com.appetiser.module.domain.models.miscellaneous

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UpdateGate(
    val id: Int,
    val version: String,
    val isRequired: Boolean,
    val title: String,
    val message: String,
    val storeUrl: String
) : Parcelable {
    companion object {
        const val EMPTY_ID = -1

        fun empty(): UpdateGate {
            return UpdateGate(
                id = EMPTY_ID,
                version = "",
                isRequired = false,
                title = "",
                message = "",
                storeUrl = ""
            )
        }
    }
}