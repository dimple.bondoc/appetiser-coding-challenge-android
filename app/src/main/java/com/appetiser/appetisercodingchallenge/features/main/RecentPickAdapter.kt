package com.appetiser.appetisercodingchallenge.features.main

import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.appetisercodingchallenge.R
import com.appetiser.appetisercodingchallenge.base.BaseListAdapter
import com.appetiser.appetisercodingchallenge.databinding.HomeSearchResultItemBinding
import com.appetiser.appetisercodingchallenge.ext.loadImageUrl
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.domain.models.searchresult.SearchResultList
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject

class RecentPickAdapter(private val disposable: CompositeDisposable) : BaseListAdapter<SearchResultList, BaseListAdapter.BaseViewViewHolder<SearchResultList>>(RecentPickDiffCallback()) {

    val itemClickListener = PublishSubject.create<SearchResultItemState>()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewViewHolder<SearchResultList> {
        return createRecentPickViewHolder(parent)
    }

    override fun onBindViewHolder(holder: BaseViewViewHolder<SearchResultList>, position: Int) {
        holder.bind(currentList[position])
    }

    private fun createRecentPickViewHolder(parent: ViewGroup): RecentPickItemViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.home_search_result_item, parent, false)

        val binding = HomeSearchResultItemBinding.bind(view)

        return RecentPickItemViewHolder(binding)
    }

    inner class RecentPickItemViewHolder(override val binding: HomeSearchResultItemBinding) : BaseViewViewHolder<SearchResultList>(binding) {
        override fun bind(item: SearchResultList) {

            with(binding) {

                imgArtWork.loadImageUrl(item.artworkUrl100)

                txtTrackName.text = item.getValidTrackName()

                txtGenre.text = item.primaryGenreName

                txtPrice.text = item.getTrackPrice()

                viewItem
                    .ninjaTap {
                        itemClickListener.onNext(SearchResultItemState.ShowDetails(item))
                    }
                    .addTo(disposable)
            }
        }
    }
}
