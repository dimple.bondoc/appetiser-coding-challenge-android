package com.appetiser.appetisercodingchallenge.features.audiobook

import android.os.Bundle
import android.view.View
import androidx.paging.LoadState
import androidx.paging.filter
import com.appetiser.appetisercodingchallenge.R
import com.appetiser.appetisercodingchallenge.base.BaseViewModelFragment
import com.appetiser.appetisercodingchallenge.databinding.FragmentAudiobookBinding
import com.appetiser.appetisercodingchallenge.features.main.*
import com.appetiser.module.common.extensions.gone
import com.appetiser.module.common.extensions.navigate
import com.appetiser.module.common.extensions.visible
import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.google.gson.Gson
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber
import javax.inject.Inject

class AudioBookFragment : BaseViewModelFragment<FragmentAudiobookBinding, MainViewModel>() {

    @Inject
    lateinit var gson: Gson

    private lateinit var adapter: SearchResultAdapter

    private val recentPickAdapter: RecentPickAdapter by lazy {
        RecentPickAdapter(disposables)
    }

    override fun getLayoutId(): Int = R.layout.fragment_audiobook

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupVmObservers()
        setupRecyclerView()
        setupRecentPickRecyclerView()
        viewModel.getAudioBookUserRecentPicks()
    }

    private fun setupVmObservers() {
        viewModel
            .searchResult
            .observe(viewLifecycleOwner) { searchResult ->
                adapter
                    .updateItems(
                        lifecycle,
                        searchResult.filter { it.wrapperType == "audiobook" }
                    )
            }
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = {
                    handleState(it)
                }
            ).addTo(disposables)
    }

    private fun handleState(state: MainState) {
        when (state) {
            is MainState.RefreshAudioBookRecentPickList -> {
                binding.textRecentPicks.visible()
                binding.rvRecentPicks.visible()
                recentPickAdapter.updateItems(state.recentPickList, true)
            }
            MainState.HideAudioBookRecentPickTitle -> {
                binding.textRecentPicks.gone()
            }
        }
    }

    private fun setupRecentPickRecyclerView() {
        binding.rvRecentPicks.adapter = recentPickAdapter

        recentPickAdapter
            .itemClickListener
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleItemClick
            )
            .addTo(disposables)
    }

    private fun setupRecyclerView() {
        adapter = SearchResultAdapter(disposables, gson)

        binding.rvAudioBooks.adapter = adapter

        adapter
            .itemClickListener
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleItemClick
            )
            .addTo(disposables)

        adapter.addLoadStateListener { loadState ->
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error

            errorState?.let {
                Timber.e("onLoadStateError: ${it.error.message}")
            }
        }
    }

    private fun handleItemClick(searchResultItemState: SearchResultItemState) {
        when (searchResultItemState) {
            is SearchResultItemState.ShowDetails -> {
                viewModel
                    .saveRecentPick(
                        searchResultItemState.item
                    )
                navigateToDetails(
                    searchResultItemState.item
                )
            }
        }
    }

    private fun navigateToDetails(item: SearchResultList) {
        navigate(
            AudioBookFragmentDirections
                .actionAudioBookFragmentToDetailsFragment(
                    item
                )
        )
    }
}
