package com.appetiser.appetisercodingchallenge.features.main.paging

import androidx.paging.PagingState
import androidx.paging.rxjava2.RxPagingSource
import com.appetiser.appetisercodingchallenge.utils.schedulers.BaseSchedulerProvider
import com.appetiser.module.data.features.searchresult.SearchResultRepository
import com.appetiser.module.domain.models.searchresult.SearchResultList
import io.reactivex.Single
import javax.inject.Inject

class MainPagingSource @Inject constructor(
    private val searchResultRepository: SearchResultRepository,
    private val schedulers: BaseSchedulerProvider
) : RxPagingSource<Int, SearchResultList>() {

    override fun loadSingle(params: LoadParams<Int>): Single<LoadResult<Int, SearchResultList>> {
        val page = params.key ?: 1
        return searchResultRepository
            .getSearchResult()
            .subscribeOn(schedulers.io())
            .map<LoadResult<Int, SearchResultList>> {
                LoadResult.Page(
                    it,
                    null,
                    null
                )
            }
            .onErrorReturn { LoadResult.Error(it) }
    }

    override fun getRefreshKey(state: PagingState<Int, SearchResultList>): Int? {
        return state.anchorPosition
    }
}
