package com.appetiser.appetisercodingchallenge.features.main

import android.view.View
import com.appetiser.module.domain.models.searchresult.SearchResultList

sealed class MainState {
    data class ShowUpdateGateForced(val storeUrl: String) : MainState()

    data class ShowUpdateGateRecommended(val storeUrl: String) : MainState()

    object ShowBottomNavigation : MainState()

    object HideBottomNavigation : MainState()

    object ChangeSoftInputModeAdjustPan : MainState()

    object ChangeSoftInputModeDefault : MainState()

    /**
     * Default status bar color.
     */
    object ChangeStatusBarToDefault : MainState()

    /**
     * Changes status bar to transparent with [View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR] flag set to false.
     */
    object ChangeStatusBarToDarkTransparent : MainState()

    /**
     * Changes status bar to transparent with [View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR] flag set to true.
     */
    object ChangeStatusBarToLightTransparent : MainState()

    object ShowNoInternetConnectionError : MainState()

    object GetAudioBookRecentPickList : MainState()

    object GetTrackRecentPickList : MainState()

    data class RefreshAudioBookRecentPickList(val recentPickList: List<SearchResultList>) : MainState()

    data class RefreshTrackRecentPickList(val recentPickList: List<SearchResultList>) : MainState()

    object HideAudioBookRecentPickTitle : MainState()

    object HideTrackRecentPickTitle : MainState()
}
