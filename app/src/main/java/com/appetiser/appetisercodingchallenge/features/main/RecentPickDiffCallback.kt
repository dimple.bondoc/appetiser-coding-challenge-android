package com.appetiser.appetisercodingchallenge.features.main

import androidx.recyclerview.widget.DiffUtil
import com.appetiser.module.domain.models.searchresult.SearchResultList

class RecentPickDiffCallback : DiffUtil.ItemCallback<SearchResultList>() {

    override fun areItemsTheSame(oldItem: SearchResultList, newItem: SearchResultList): Boolean {
        return oldItem.stampDate == newItem.stampDate
    }

    override fun areContentsTheSame(oldItem: SearchResultList, newItem: SearchResultList): Boolean {
        return oldItem == newItem && oldItem.stampDate == newItem.stampDate
    }
}
