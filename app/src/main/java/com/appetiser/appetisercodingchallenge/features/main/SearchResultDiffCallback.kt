package com.appetiser.appetisercodingchallenge.features.main

import androidx.recyclerview.widget.DiffUtil
import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.google.gson.Gson

class SearchResultDiffCallback(private val gson: Gson) : DiffUtil.ItemCallback<SearchResultList>() {

    override fun areItemsTheSame(oldItem: SearchResultList, newItem: SearchResultList): Boolean {
        return oldItem.artistId == newItem.artistId || oldItem.trackId == newItem.trackId
    }

    override fun areContentsTheSame(oldItem: SearchResultList, newItem: SearchResultList): Boolean {
        return gson.toJson(oldItem) == gson.toJson(newItem)
    }
}
