package com.appetiser.appetisercodingchallenge.features.detail

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.appetiser.appetisercodingchallenge.R
import com.appetiser.appetisercodingchallenge.base.BaseFragment
import com.appetiser.appetisercodingchallenge.databinding.FragmentDetailBinding
import com.appetiser.appetisercodingchallenge.ext.loadImageUrl

class DetailFragment : BaseFragment<FragmentDetailBinding>() {

    override fun getLayoutId(): Int = R.layout.fragment_detail
    private val args: DetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupViews()
        setupToolbar()
    }

    private fun setupViews() {
        val detail = args.detail

        with(binding) {
            imgArtWork.loadImageUrl(detail.artworkUrl100)
            txtTrackName.text = detail.getValidTrackName()
            txtGenre.text = detail.primaryGenreName
            btnPrice.text = detail.getTrackPrice()
            txtArtistName.text = detail.artistName
            txtAbout.text = "About this ${detail.wrapperType}"
            txtLongDescription.text = detail.getValidDescription()
        }
    }

    private fun setupToolbar() {
        enableToolbarHomeIndicator(binding.toolbarView)
    }

    override fun canBack(): Boolean {
        return true
    }
}
