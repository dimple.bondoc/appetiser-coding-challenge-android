package com.appetiser.appetisercodingchallenge.features.main

import android.os.Bundle
import androidx.lifecycle.viewModelScope
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.cachedIn
import androidx.paging.liveData
import com.appetiser.appetisercodingchallenge.R
import com.appetiser.appetisercodingchallenge.base.BaseViewModel
import com.appetiser.appetisercodingchallenge.features.main.paging.MainPagingSource
import com.appetiser.module.data.features.searchresult.SearchResultRepository
import com.appetiser.module.data.features.session.SessionRepository
import com.appetiser.module.domain.models.miscellaneous.UpdateGate
import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.appetiser.module.domain.models.session.Session
import com.appetiser.module.domain.utils.RECOMMEND_UPDATE_INTERVAL_IN_MILLIS
import io.reactivex.Observable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val sessionRepository: SessionRepository,
    private val searchResultRepository: SearchResultRepository
) : BaseViewModel() {

    private val _state by lazy {
        PublishSubject.create<MainState>()
    }

    val state: Observable<MainState> = _state

    private val pager by lazy {
        Pager(
            config = PagingConfig(
                pageSize = 10,
                enablePlaceholders = false,
                initialLoadSize = 10,
                prefetchDistance = 1
            ),
            pagingSourceFactory = {
                MainPagingSource(searchResultRepository, schedulers)
            }
        )
    }

    val searchResult = pager
        .liveData
        .cachedIn(viewModelScope)

    private val bottomNavDestinationIds by lazy {
        arrayOf(
            R.id.homeFragment,
            R.id.audioBookFragment
        )
    }

    override fun isFirstTimeUiCreate(bundle: Bundle?) = Unit

    fun versionCheck() {
        sessionRepository
            .getSession()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onSuccess = { session ->
                    if (session.updateGate == null) return@subscribeBy

                    handleUpdateGate(session.updateGate!!, session)
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun onUpdateLaterClicked() {
        sessionRepository
            .getSession()
            .flatMap { session ->
                session.recommendUpdateTimeStamp = System.currentTimeMillis()

                sessionRepository.saveSession(session)
            }
            .ignoreElement()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onComplete = {},
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun saveRecentPick(item: SearchResultList) {
        item.stampDate = Date().time
        searchResultRepository
            .insert(item)
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onComplete = {
                    Timber.d("Saved successfully.")
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun getAudioBookUserRecentPicks() {
        searchResultRepository
            .getAudioBooks().toObservable()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    if (it.isNullOrEmpty()) {
                        _state
                            .onNext(
                                MainState.HideAudioBookRecentPickTitle
                            )
                    } else {
                        _state
                            .onNext(
                                MainState.RefreshAudioBookRecentPickList(it)
                            )
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    fun getTrackUserRecentPicks() {
        searchResultRepository
            .getTracks().toObservable()
            .subscribeOn(schedulers.io())
            .observeOn(schedulers.ui())
            .subscribeBy(
                onNext = {
                    if (it.isNullOrEmpty()) {
                        Timber.d("Empty list")
                        _state
                            .onNext(
                                MainState.HideTrackRecentPickTitle
                            )
                    } else {
                        Timber.d("${it.size}")
                        _state
                            .onNext(
                                MainState.RefreshTrackRecentPickList(it)
                            )
                    }
                },
                onError = {
                    Timber.e(it)
                }
            )
            .addTo(disposables)
    }

    private fun handleUpdateGate(updateGate: UpdateGate, session: Session) {
        if (updateGate.isRequired) {
            _state.onNext(
                MainState.ShowUpdateGateForced(
                    updateGate.storeUrl
                )
            )
        } else {
            val recommendUpdateTimeElapsed = System.currentTimeMillis() - session.recommendUpdateTimeStamp
            val shouldRecommendUpdate = recommendUpdateTimeElapsed >= RECOMMEND_UPDATE_INTERVAL_IN_MILLIS

            // If recommend update has not yet passed interval time.
            if (!shouldRecommendUpdate) return

            _state.onNext(
                MainState.ShowUpdateGateRecommended(
                    updateGate.storeUrl
                )
            )
        }
    }

    fun onDestinationChanged(destinationId: Int) {
        handleBottomNavigation(destinationId)
    }

    private fun handleBottomNavigation(destinationId: Int) {
        when (destinationId) {
            in bottomNavDestinationIds -> {
                _state
                    .onNext(
                        MainState.ShowBottomNavigation
                    )
            }
            else -> {
                _state
                    .onNext(
                        MainState.HideBottomNavigation
                    )
            }
        }
    }

    fun onIoExceptionReceived() {
        _state.onNext(MainState.ShowNoInternetConnectionError)
    }
}
