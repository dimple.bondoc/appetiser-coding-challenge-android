package com.appetiser.appetisercodingchallenge.features.main

import android.view.LayoutInflater
import android.view.ViewGroup
import com.appetiser.appetisercodingchallenge.R
import com.appetiser.appetisercodingchallenge.base.BasePagingAdapter
import com.appetiser.appetisercodingchallenge.databinding.HomeSearchResultItemBinding
import com.appetiser.appetisercodingchallenge.ext.loadImageUrl
import com.appetiser.module.common.extensions.ninjaTap
import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.google.gson.Gson
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.PublishSubject

class SearchResultAdapter(
    private val disposable: CompositeDisposable,
    gson: Gson
) : BasePagingAdapter<SearchResultList, BasePagingAdapter.BaseViewHolder<SearchResultList>>(SearchResultDiffCallback(gson)) {

    val itemClickListener = PublishSubject.create<SearchResultItemState>()

    override fun onBindViewHolder(holder: BaseViewHolder<SearchResultList>, position: Int) {
        getItem(position)?.let {
            holder.itemPosition = position
            holder.bind(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<SearchResultList> {
        return createSearchResultViewHolder(parent)
    }

    private fun createSearchResultViewHolder(parent: ViewGroup): SearchResultViewHolder {
        val view =
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.home_search_result_item, parent, false)

        val binding = HomeSearchResultItemBinding.bind(view)

        return SearchResultViewHolder(binding)
    }

    inner class SearchResultViewHolder(override val binding: HomeSearchResultItemBinding) : BaseViewHolder<SearchResultList>(binding) {
        override fun bind(item: SearchResultList) {
            super.bind(item)

            with(binding) {

                imgArtWork.loadImageUrl(item.artworkUrl100)

                txtTrackName.text = item.getValidTrackName()

                txtGenre.text = item.primaryGenreName

                txtPrice.text = item.getTrackPrice()

                viewItem
                    .ninjaTap {
                        itemClickListener.onNext(SearchResultItemState.ShowDetails(item))
                    }
                    .addTo(disposable)
            }
        }
    }
}

sealed class SearchResultItemState {
    data class ShowDetails(val item: SearchResultList) : SearchResultItemState()
}
