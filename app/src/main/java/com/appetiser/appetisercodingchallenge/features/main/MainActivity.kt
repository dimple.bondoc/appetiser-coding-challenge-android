package com.appetiser.appetisercodingchallenge.features.main

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import androidx.annotation.ColorInt
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.appetiser.appetisercodingchallenge.R
import com.appetiser.appetisercodingchallenge.base.BaseViewModelActivity
import com.appetiser.appetisercodingchallenge.databinding.ActivityMainBinding
import com.appetiser.appetisercodingchallenge.ext.getThemeColor
import com.appetiser.appetisercodingchallenge.ext.makeSoftInputModeAdjustPan
import com.appetiser.appetisercodingchallenge.ext.makeSoftInputModeAdjustResize
import com.appetiser.appetisercodingchallenge.utils.ViewUtils
import com.appetiser.module.common.extensions.makeStatusBarNonTransparent
import com.appetiser.module.common.extensions.makeStatusBarTransparent
import com.appetiser.module.common.extensions.setVisible
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class MainActivity : BaseViewModelActivity<ActivityMainBinding, MainViewModel>() {
    companion object {
        const val EXTRA_START_SCREEN = "EXTRA_START_SCREEN"
        const val EXTRA_EMAIL = "EXTRA_EMAIL"
        const val EXTRA_PHONE = "EXTRA_PHONE"

        const val START_MAIN = "START_MAIN"
        const val START_VERIFICATION = "START_VERIFICATION"
        const val START_UPLOAD_PROFILE_PHOTO = "START_UPLOAD_PROFILE_PHOTO"

        fun openActivity(context: Context, extras: Bundle) {
            context.startActivity(
                Intent(
                    context,
                    MainActivity::class.java
                ).apply {
                    putExtras(extras)
                }
            )
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupVmObservers()
        setupViews()
        setupBroadcastReceivers()
    }

    override fun onDestroy() {
        unregisterReceiver(ioExceptionReceiver)
        super.onDestroy()
    }

    private val ioExceptionReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            viewModel.onIoExceptionReceived()
        }
    }

    private fun setupBroadcastReceivers() {
        registerReceiver(
            ioExceptionReceiver,
            IntentFilter(getString(R.string.intent_action_io_exception))
        )
    }

    override fun onResume() {
        super.onResume()
        viewModel.versionCheck()
    }

    private fun setupViews() {
        navController().addOnDestinationChangedListener { _, destination, _ ->
            viewModel.onDestinationChanged(destination.id)
        }

        binding.bottomNavigation.apply {
            setupWithNavController(navController())
            setOnNavigationItemReselectedListener { /* do nothing */ }

            // https://github.com/material-components/material-components-android/issues/499
            setOnApplyWindowInsetsListener(null)
        }
    }

    private fun setupVmObservers() {
        viewModel
            .state
            .observeOn(scheduler.ui())
            .subscribeBy(
                onNext = ::handleState,
                onError = {
                    Timber.e(it)
                }
            )
            .apply { disposables.add(this) }
    }

    private fun handleState(state: MainState) {
        when (state) {
            MainState.ShowBottomNavigation -> {
                binding.bottomNavigation setVisible true
            }
            MainState.HideBottomNavigation -> {
                binding.bottomNavigation setVisible false
            }
            MainState.ChangeStatusBarToDarkTransparent -> {
                makeStatusBarTransparent(false)
            }
            MainState.ChangeStatusBarToLightTransparent -> {
                makeStatusBarTransparent(true)
            }
            MainState.ChangeStatusBarToDefault -> {
                makeStatusBarNonTransparent()
                changeStatusBarColor(getThemeColor(android.R.attr.colorBackground))
            }
            MainState.ChangeSoftInputModeAdjustPan -> {
                makeSoftInputModeAdjustPan()
            }
            MainState.ChangeSoftInputModeDefault -> {
                makeSoftInputModeAdjustResize()
            }
            is MainState.ShowUpdateGateForced -> {
                ViewUtils.showUpdateGate(
                    this,
                    state.storeUrl,
                    true
                )
            }
            is MainState.ShowUpdateGateRecommended -> {
                ViewUtils.showUpdateGate(
                    this,
                    state.storeUrl,
                    false,
                    updateLaterClickListener = {
                        viewModel.onUpdateLaterClicked()
                    }
                )
            }
            MainState.ShowNoInternetConnectionError -> {
                ViewUtils.showGenericErrorSnackBar(
                    binding.root,
                    getString(R.string.no_internet_connection_error)
                )
            }
        }
    }

    private fun changeStatusBarColor(@ColorInt color: Int) {
        if (window.statusBarColor != color) {
            window.statusBarColor = color
        }
    }

    private fun navigate(navDirections: NavDirections, destinationScreenId: Int) {
        if (navController().currentDestination!!.id != destinationScreenId) {
            navController()
                .navigate(navDirections)
        }
    }

    fun navController() = findNavController(R.id.nav_host_fragment)
}
