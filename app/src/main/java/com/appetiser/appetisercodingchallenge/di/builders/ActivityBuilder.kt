package com.appetiser.appetisercodingchallenge.di.builders

import com.appetiser.appetisercodingchallenge.di.scopes.ActivityScope
import com.appetiser.appetisercodingchallenge.features.main.MainActivity
import com.appetiser.appetisercodingchallenge.features.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
