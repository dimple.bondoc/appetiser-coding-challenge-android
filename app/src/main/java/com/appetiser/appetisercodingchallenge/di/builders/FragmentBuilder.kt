package com.appetiser.appetisercodingchallenge.di.builders

import android.webkit.WebViewFragment
import com.appetiser.appetisercodingchallenge.di.scopes.FragmentScope
import com.appetiser.appetisercodingchallenge.features.audiobook.AudioBookFragment
import com.appetiser.appetisercodingchallenge.features.detail.DetailFragment
import com.appetiser.appetisercodingchallenge.features.track.TrackFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilder {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeFeedsFragment(): TrackFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeAudioBookFragment(): AudioBookFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeDetailFragment(): DetailFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeWebViewFragment(): WebViewFragment
}
