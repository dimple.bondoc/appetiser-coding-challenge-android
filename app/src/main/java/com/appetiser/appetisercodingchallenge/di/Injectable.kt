package com.appetiser.appetisercodingchallenge.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
