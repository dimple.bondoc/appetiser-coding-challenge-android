package com.appetiser.appetisercodingchallenge.di

import com.appetiser.appetisercodingchallenge.utils.schedulers.BaseSchedulerProvider
import com.appetiser.appetisercodingchallenge.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    fun providesSchedulerSource(): BaseSchedulerProvider =
            SchedulerProvider.getInstance()
}
