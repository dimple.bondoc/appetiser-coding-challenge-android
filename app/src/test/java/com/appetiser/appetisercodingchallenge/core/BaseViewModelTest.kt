package com.appetiser.appetisercodingchallenge.core

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.appetiser.baseplate.core.TestSchedulerProvider
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Rule

open class BaseViewModelTest {
    @get:Rule
    val executor = InstantTaskExecutorRule()

    val testScheduler = TestScheduler()
    val schedulers = TestSchedulerProvider(testScheduler)

    @Before
    fun baseSetUp() {
        RxJavaPlugins.setComputationSchedulerHandler { testScheduler }
    }
}
