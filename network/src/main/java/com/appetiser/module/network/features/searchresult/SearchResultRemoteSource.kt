package com.appetiser.module.network.features.searchresult

import com.appetiser.module.domain.models.searchresult.SearchResultList
import io.reactivex.Single

interface SearchResultRemoteSource {
    fun getSearchResult(): Single<List<SearchResultList>>
}
