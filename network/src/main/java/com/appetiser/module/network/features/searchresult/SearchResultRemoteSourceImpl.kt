package com.appetiser.module.network.features.searchresult

import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.appetiser.module.network.base.BaseRemoteSource
import com.appetiser.module.network.features.BaseplateApiServices
import com.appetiser.module.network.features.searchresult.models.SearchResultListDTO
import io.reactivex.Single
import javax.inject.Inject

class SearchResultRemoteSourceImpl @Inject constructor(
    private val apiServices: BaseplateApiServices
) : BaseRemoteSource(), SearchResultRemoteSource {

    override fun getSearchResult(): Single<List<SearchResultList>> {
        return apiServices
            .getSearchResult()
            .map { it.results }
            .map { SearchResultListDTO.mapFromSearchResultList(it) }
    }
}
