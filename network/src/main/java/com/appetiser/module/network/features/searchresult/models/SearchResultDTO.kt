package com.appetiser.module.network.features.searchresult.models

open class SearchResultDTO(
    var resultCount: Int? = null,
    var results: List<SearchResultListDTO>? = null
)
