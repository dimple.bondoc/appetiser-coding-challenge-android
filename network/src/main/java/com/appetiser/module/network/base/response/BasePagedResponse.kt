package com.appetiser.module.network.base.response

data class BasePagedResponse<T>(
    val data: List<T>,
    val meta: PagingMeta
) : BaseResponse()
