package com.appetiser.module.network.features.searchresult.models

import com.appetiser.module.domain.models.searchresult.SearchResultList

class SearchResultListDTO(
    var wrapperType: String? = null,
    var kind: String? = null,
    var artistId: Int? = null,
    var collectionId: Int? = null,
    var trackId: Int? = null,
    var artistName: String? = null,
    var collectionName: String? = null,
    var trackName: String? = null,
    var collectionCensoredName: String? = null,
    var trackCensoredName: String? = null,
    var artworkUrl100: String? = null,
    var collectionPrice: Double? = null,
    var trackPrice: Double? = null,
    var releaseDate: String? = null,
    var collectionExplicitness: String? = null,
    var trackExplicitness: String? = null,
    var discCount: Int? = null,
    var discNumber: Int? = null,
    var trackCount: Int? = null,
    var trackNumber: Int? = null,
    var trackTimeMillis: Int? = null,
    var country: String? = null,
    var currency: String? = null,
    var primaryGenreName: String? = null,
    var description: String? = null,
    var longDescription: String? = null

) {

    companion object {
        fun fromDomain(recentPick: SearchResultListDTO): SearchResultList {
            return with(recentPick) {
                SearchResultList(
                    wrapperType = recentPick.wrapperType,
                    kind = recentPick.kind,
                    artistId = recentPick.artistId,
                    collectionId = recentPick.collectionId,
                    trackId = recentPick.trackId,
                    artistName = recentPick.artistName,
                    collectionName = recentPick.collectionName,
                    trackName = recentPick.trackName,
                    collectionCensoredName = recentPick.collectionCensoredName,
                    trackCensoredName = recentPick.trackCensoredName,
                    artworkUrl100 = recentPick.artworkUrl100,
                    collectionPrice = recentPick.collectionPrice,
                    trackPrice = recentPick.trackPrice,
                    releaseDate = recentPick.releaseDate,
                    collectionExplicitness = recentPick.collectionExplicitness,
                    trackExplicitness = recentPick.trackExplicitness,
                    discCount = recentPick.discCount,
                    discNumber = recentPick.discNumber,
                    trackCount = recentPick.trackCount,
                    trackNumber = recentPick.trackNumber,
                    trackTimeMillis = recentPick.trackTimeMillis,
                    country = recentPick.country,
                    currency = recentPick.currency,
                    primaryGenreName = recentPick.primaryGenreName,
                    description = recentPick.description,
                    longDescription = recentPick.longDescription
                )
            }
        }

        fun toDomain(recentPickDB: SearchResultListDTO): SearchResultList {
            return with(recentPickDB) {
                SearchResultList(
                    wrapperType = wrapperType,
                    kind = kind,
                    artistId = artistId,
                    collectionId = collectionId,
                    trackId = trackId,
                    artistName = artistName,
                    collectionName = collectionName,
                    trackName = trackName,
                    collectionCensoredName = collectionCensoredName,
                    trackCensoredName = trackCensoredName,
                    artworkUrl100 = artworkUrl100,
                    collectionPrice = collectionPrice,
                    trackPrice = trackPrice,
                    releaseDate = releaseDate,
                    collectionExplicitness = collectionExplicitness,
                    trackExplicitness = trackExplicitness,
                    discCount = discCount,
                    discNumber = discNumber,
                    trackCount = trackCount,
                    trackNumber = trackNumber,
                    trackTimeMillis = trackTimeMillis,
                    country = country,
                    currency = currency,
                    primaryGenreName = primaryGenreName,
                    description = description,
                    longDescription = longDescription
                )
            }
        }

        fun mapFromSearchResultList(searchResultList: List<SearchResultListDTO>): List<SearchResultList> {
            return searchResultList.map { fromDomain(it) }
        }
    }
}
