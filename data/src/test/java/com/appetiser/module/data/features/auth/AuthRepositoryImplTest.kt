package com.appetiser.module.data.features.auth

import com.appetiser.module.data.features.Stubs
import com.appetiser.module.domain.utils.any
import com.appetiser.module.domain.utils.mock
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.auth.AuthRemoteSource
import com.appetiser.module.network.features.user.UserRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.Mockito.times

class AuthRepositoryImplTest {

    private val authRemoteSource: AuthRemoteSource = mock()
    private val sessionLocalSource: SessionLocalSource = mock()
    private val userRemoteSource: UserRemoteSource = mock()

    private lateinit var subject: AuthRepository

    @Before
    fun setUp() {
        subject = AuthRepositoryImpl(
            authRemoteSource,
            sessionLocalSource,
            userRemoteSource
        )
    }

    @Test
    fun logout_ShouldCallRemoteAndClearSession_WhenExecuted() {
        val session = Stubs.SESSION_LOGGED_IN

        `when`(sessionLocalSource.getSession())
            .thenReturn(Single.just(session))
        `when`(authRemoteSource.logout(any()))
            .thenReturn(Completable.complete())
        `when`(sessionLocalSource.clearSession())
            .thenReturn(Completable.complete())

        subject
            .logout()
            .test()
            .assertComplete()

        Mockito
            .verify(
                sessionLocalSource,
                times(1)
            )
            .getSession()

        Mockito
            .verify(
                authRemoteSource,
                times(1)
            )
            .logout(any())

        Mockito
            .verify(
                sessionLocalSource,
                times(1)
            )
            .clearSession()
    }
}
