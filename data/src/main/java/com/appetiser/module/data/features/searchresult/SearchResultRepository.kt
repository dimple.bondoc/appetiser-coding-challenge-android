package com.appetiser.module.data.features.searchresult

import com.appetiser.module.domain.models.searchresult.SearchResultList
import io.reactivex.Completable
import io.reactivex.Single

interface SearchResultRepository {
    fun insert(searchResultList: SearchResultList): Completable

    fun getSearchResult(): Single<List<SearchResultList>>

    fun getAudioBooks(): Single<List<SearchResultList>>

    fun getTracks(): Single<List<SearchResultList>>
}
