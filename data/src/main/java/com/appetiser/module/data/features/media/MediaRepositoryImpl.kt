package com.appetiser.module.data.features.media

import com.appetiser.module.domain.models.user.MediaFile
import com.appetiser.module.local.features.session.SessionLocalSource
import com.appetiser.module.network.features.media.MediaRemoteSource
import io.reactivex.Single
import javax.inject.Inject

class MediaRepositoryImpl @Inject constructor(
    private val sessionLocalSource: SessionLocalSource,
    private val mediaRemoteSource: MediaRemoteSource
) : MediaRepository {

    override fun uploadMedia(filePath: String): Single<MediaFile> {
        return sessionLocalSource
            .getSession()
            .flatMap { session ->
                mediaRemoteSource
                    .uploadMedia(
                        session.accessToken,
                        filePath
                    )
            }
    }
}
