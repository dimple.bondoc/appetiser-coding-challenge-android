package com.appetiser.module.data.features.searchresult

import com.appetiser.module.domain.models.searchresult.SearchResultList
import com.appetiser.module.local.features.recentpick.RecentPickLocalSource
import com.appetiser.module.network.features.searchresult.SearchResultRemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class SearchResultRepositoryImpl @Inject constructor(
    private val recentPickLocalSource: RecentPickLocalSource,
    private val searchResultRemoteSource: SearchResultRemoteSource
) : SearchResultRepository {
    override fun insert(searchResultList: SearchResultList): Completable {
        return recentPickLocalSource.saveRecentPick(searchResultList)
    }

    override fun getSearchResult(): Single<List<SearchResultList>> {
        return searchResultRemoteSource
            .getSearchResult()
    }

    override fun getAudioBooks(): Single<List<SearchResultList>> {
        return recentPickLocalSource.getAudioBooks()
    }

    override fun getTracks(): Single<List<SearchResultList>> {
        return recentPickLocalSource.getTracks()
    }
}
